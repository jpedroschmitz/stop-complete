console.log('ready');
$('.btn-connect').click(function () {
    console.log('entrou');
    let letra = $('#letra').val();
    let url = 'http://dicionario-aberto.net/search-json?prefix=' + letra;
    console.log(url);
    $.ajax({
        url: url,
        global: false,
        type: "POST",
        contentType: "application/x-www-form-urlencoded;charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            showLoader();
        },
        success: function (data) {
            console.log('entrou no sucesso');
        },
        complete: function () {
            hideLoader();
        }
    }).responseText;
});

function showLoader() {
    $('.loading').show();
}

function hideLoader() {
    $('.loading').hide();
}